#!/bin/bash

# Disable SELinux
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

# Configure firewall rules
sudo systemctl enable firewalld
sudo systemctl start firewalld
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=2379-2380/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10252/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --reload

# Disable swap
sudo swapoff -a
sudo sed -i '/swap/d' /etc/fstab

# Install Docker
sudo yum install -y docker
sudo systemctl enable docker
sudo systemctl start docker

# Install kubeadm, kubelet, and kubectl
sudo tee /etc/yum.repos.d/kubernetes.repo <<EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

sudo yum install -y kubelet kubeadm kubectl
sudo systemctl enable kubelet
sudo systemctl start kubelet

# Initialize the Kubernetes cluster
sudo kubeadm init

# Set up the kubeconfig for the current user
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Apply pod network overlay

kubectl apply -f "https://github.com/projectcalico/calico/blob/master/manifests/calico.yaml"

# Print the join command to join worker nodes to the cluster  
# When you run this script, it will generate the join command and save it to a file named join-key.txt. 
# You can then share the contents of the file with other nodes that you want to join to the Kubernetes cluster.
#To Display the content of the join-key.txt, you can run the command below:
#cat join-key.txt

kubeadm token create --print-join-command > join-key.txt

# Print the cluster information
kubectl cluster-info

# Print the status of the cluster components
kubectl get componentstatuses

# Print the status of the nodes
kubectl get nodes


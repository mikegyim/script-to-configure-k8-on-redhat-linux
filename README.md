
This is for running online.


To run the script, you can follow these steps:

Open a text editor on your Red Hat Linux machine and copy the script into a new file.

You can also clone to your local machine.

Save the file with a descriptive name, such as k8s_setup.sh.

Open a terminal or command prompt on your Red Hat Linux machine.

Navigate to the directory where you saved the script using the cd command. 


Give the script execute permissions by running the following command:

chmod +x k8s_setup.sh


Run the script using the following command:

./k8s_setup.sh

